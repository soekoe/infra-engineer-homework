#!/usr/bin/env bash
environment=${1:-dev}

aws elasticbeanstalk update-environment \
    --application-name "streamlit-${environment}" \
    --environment-name "streamlit-${environment}-env" \
    --version-label "streamlit-${environment}-version"
