# alcemy Infrastructure Engineer Homework

## Setup

This app uses Streamlit, a web app framework for Python. In order to run it, make sure you have a recent version of Python installed. Then use pip to install pipenv and use that to install this app's dependencies like so: `pipenv install`.

## Running

The app can then be started simply by calling `streamlit run homework/app.py`. This will start the interpreter and open port 8501 which can then be accessed locally.

## Testing

Tests (not yet fully implemented) can be run simply by typing `pytest`.

## Infrastructure

The infrastructure is managed in `infra/` subfolder, there is also a Makefile with convenience targets for Terraform.
In order to create the infrastructure, following credentials have to be provided in `infra/terraform.tfvars` or some other Terraform variable mechanism:
- GitLab API token as `gitlab_token`
- an authentication password for the web UI as `streamlit_password`

## Deployment

Each commit to this repository will trigger a pipeline that publishes a new Docker image to the ECR.
With the [AWS CLI](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3-install.html) installed, and credentials set accordingly, you are able to deploy this last built Docker image with:

```
./deploy.sh
```
