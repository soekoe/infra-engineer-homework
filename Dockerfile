FROM python:3.11 as builder

RUN python -m pip install pipenv

COPY Pipfile Pipfile.lock .
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install

FROM python:3.11-slim-bookworm

COPY --from=builder /.venv/ /.venv/
ENV PATH="/.venv/bin:$PATH"

RUN useradd --create-home appuser
WORKDIR /home/appuser
USER appuser

COPY homework/ homework/
COPY some_database.sqlite some_database.sqlite

HEALTHCHECK CMD curl --fail http://localhost:8501/_stcore/health

EXPOSE 8501

ENTRYPOINT ["streamlit"]
CMD ["run", "./homework/app.py", "--browser.serverAddress=0.0.0.0" ]
