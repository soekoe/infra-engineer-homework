output "aws_ecr_repository" {
  value = aws_ecr_repository.image_repository.repository_url
}

output "aws_record_lb" {
  value = aws_route53_record.lb.name
}
