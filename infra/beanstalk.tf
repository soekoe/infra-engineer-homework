resource "random_id" "ebs_bucket_name" {
  byte_length = 4
}

resource "aws_s3_bucket" "ebs" {
  bucket = "${local.common_name}-${random_id.ebs_bucket_name.hex}"
}

resource "aws_s3_object" "ebs_deployment" {
  bucket  = aws_s3_bucket.ebs.id
  key     = "Dockerrun.aws.json"
  content = <<-EOF
  {
    "AWSEBDockerrunVersion": "1",
    "Image": {
      "Name": "${aws_ecr_repository.image_repository.repository_url}",
      "Update": "true"
    },
    "Ports": [
      {
        "ContainerPort": "8501",
        "HostPort": "8501"
      }
    ]
  }
  EOF
}

resource "aws_elastic_beanstalk_application" "app" {
  name = local.common_name
}

resource "aws_elastic_beanstalk_application_version" "app_version" {
  name        = "${local.common_name}-version"
  application = aws_elastic_beanstalk_application.app.name
  bucket      = aws_s3_bucket.ebs.id
  key         = aws_s3_object.ebs_deployment.id
}

resource "aws_elastic_beanstalk_environment" "env" {
  name                   = "${local.common_name}-env"
  application            = aws_elastic_beanstalk_application.app.name
  version_label          = aws_elastic_beanstalk_application_version.app_version.name
  solution_stack_name    = "64bit Amazon Linux 2023 v4.2.1 running Docker"
  tier                   = "WebServer"
  wait_for_ready_timeout = "10m"

  setting {
    name      = "Application Healthcheck URL"
    namespace = "aws:elasticbeanstalk:application"
    resource  = ""
    value     = "/_stcore/health"
  }

  setting {
    name      = "InstanceTypes"
    namespace = "aws:ec2:instances"
    resource  = ""
    value     = var.instance_type
  }

  setting {
    name      = "ListenerEnabled"
    namespace = "aws:elb:listener:80"
    resource  = ""
    value     = "false"
  }

  setting {
    name      = "IamInstanceProfile"
    namespace = "aws:autoscaling:launchconfiguration"
    resource  = ""
    value     = aws_iam_instance_profile.beanstalk_iam_instance_profile.name
  }

  setting {
    name      = "LoadBalancerType"
    namespace = "aws:elasticbeanstalk:environment"
    resource  = ""
    value     = "classic"
  }

  setting {
    name      = "InstanceProtocol"
    namespace = "aws:elb:listener:443"
    resource  = ""
    value     = "TCP"
  }

  setting {
    name      = "ListenerProtocol"
    namespace = "aws:elb:listener:443"
    resource  = ""
    value     = "SSL"
  }

  setting {
    name      = "InstancePort"
    namespace = "aws:elb:listener:443"
    resource  = ""
    value     = "80"
  }

  setting {
    name      = "SSLCertificateId"
    namespace = "aws:elb:listener:443"
    resource  = ""
    value     = aws_acm_certificate.cert.id
  }

  setting {
    name      = "VPCId"
    namespace = "aws:ec2:vpc"
    resource  = ""
    value     = module.vpc.vpc_id
  }

  setting {
    name      = "Subnets"
    namespace = "aws:ec2:vpc"
    resource  = ""
    value     = join(",", sort(module.subnets.public_subnet_ids))
  }

  setting {
  name      = "STREAMLIT_PASSWORD"
  namespace = "aws:elasticbeanstalk:application:environment"
  resource  = ""
  value     = var.streamlit_password
  }
}
