variable "region" {
  type        = string
  description = "AWS Region"
  default     = "eu-central-1"
}

variable "gitlab_token" {
  type        = string
  description = "token for GitLab API"
  sensitive   = true
}

variable "instance_type" {
  type    = string
  default = "t3.micro"
}

variable "domain_name" {
  type        = string
  description = "Fully qualified domain name of the primary domain."
}

variable "streamlit_password" {
  type        = string
  description = "Password for Streamlit web interface."
  sensitive   = true
}
