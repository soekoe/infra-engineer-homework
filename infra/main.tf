terraform {
  required_version = ">= 1.7.1"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 16.8"
    }
  }

  backend "s3" {
    bucket = "alcemy-terraform-state"
    key    = "state"
    region = "eu-central-1"
  }
}

provider "aws" {
  region = var.region
}

provider "gitlab" {
  token = var.gitlab_token
}

locals {
  common_name = "${var.name}-${var.environment}"
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "alcemy-terraform-state"
}
