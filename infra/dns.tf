locals {
  app_name = var.environment == "prod" ? var.name : "${var.name}-${var.environment}"
}

resource "aws_route53_zone" "primary" {
  name = var.domain_name
}

import {
  id = "Z02258411PY9BT2YQYU08"
  to = aws_route53_zone.primary
}

resource "aws_route53_record" "record" {
  for_each = {
    for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  name            = each.value.name
  records         = [each.value.record]
  type            = each.value.type
  zone_id         = aws_route53_zone.primary.zone_id
  ttl             = 60
  allow_overwrite = true
}

resource "aws_acm_certificate" "cert" {
  domain_name               = var.domain_name
  validation_method         = "DNS"
  subject_alternative_names = ["*.${var.domain_name}"]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate_validation" "validation" {
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = [for record in aws_route53_record.record : record.fqdn]

  timeouts {
    create = "90m"
  }
}

resource "aws_route53_record" "lb" {
  zone_id = aws_route53_zone.primary.zone_id
  name    = "${local.app_name}.${var.domain_name}"
  type    = "CNAME"
  ttl     = 5
  records = [aws_elastic_beanstalk_environment.env.cname]
}
