resource "aws_ecr_repository" "image_repository" {
  name = var.name

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_iam_user" "ecr_user" {
  name = "ecr-docker-push-user"
}

resource "aws_iam_policy" "ecr_policy" {
  name        = "ECRPushPolicy"
  description = "Policy for pushing Docker images to ECR"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = [
          "ecr:GetAuthorizationToken",
          "ecr:BatchCheckLayerAvailability",
          "ecr:CompleteLayerUpload",
          "ecr:InitiateLayerUpload",
          "ecr:PutImage",
          "ecr:UploadLayerPart"
        ],
        Resource = "*"
      }
    ]
  })
}

resource "aws_iam_user_policy_attachment" "ecr_user_policy_attachment" {
  user       = aws_iam_user.ecr_user.name
  policy_arn = aws_iam_policy.ecr_policy.arn
}

resource "aws_iam_access_key" "ecr_user_key" {
  user = aws_iam_user.ecr_user.name
}

import {
  to = gitlab_project.infra_engineer_homework
  id = "soekoe/infra-engineer-homework"
}

resource "gitlab_project" "infra_engineer_homework" {
  name                   = "infra-engineer-homework"
  forked_from_project_id = "52273416"
}

resource "gitlab_project_variable" "aws_access_key_id" {
  project = gitlab_project.infra_engineer_homework.id
  key     = "AWS_ACCESS_KEY_ID"
  value   = aws_iam_access_key.ecr_user_key.id
  masked  = true
}

resource "gitlab_project_variable" "aws_secret_access_key" {
  project = gitlab_project.infra_engineer_homework.id
  key     = "AWS_SECRET_ACCESS_KEY"
  value   = aws_iam_access_key.ecr_user_key.secret
  masked  = true
}
