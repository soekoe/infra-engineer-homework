import os
import hmac
import streamlit as st

from database.model import Customer, engine
from sqlalchemy.orm import Session
from sqlalchemy import select

st.set_page_config(
    page_title="My first app",
    layout="wide",
    initial_sidebar_state="expanded",
    menu_items={
        "Get help": None,
        "Report a bug": None,
        "About": "alcemy infra homework",
    },
)

def check_password(configured_password):
    """Returns `True` if the user had the correct password."""

    def password_entered():
        """Checks whether a password entered by the user is correct."""
        if hmac.compare_digest(st.session_state["password"], configured_password):
            st.session_state["password_correct"] = True
            del st.session_state["password"]  # Don't store the password.
        else:
            st.session_state["password_correct"] = False

    # Return True if the password is validated.
    if st.session_state.get("password_correct", False):
        return True

    # Show input for password.
    st.text_input(
        "Password", type="password", on_change=password_entered, key="password"
    )
    if "password_correct" in st.session_state:
        st.error("😕 Password incorrect")
    return False

def load_data():
  with Session(engine) as session:
    query = select(Customer)
    customers = session.execute(query).scalars().all()
    customer_list = [customer.__dict__ for customer in customers]
    _ = [customer.pop("_sa_instance_state") for customer in customer_list]  # lord please forgive me
    return customer_list


if __name__ == "__main__":
  if os.environ.get('STREAMLIT_PASSWORD') is None:
    raise("STREAMLIT_PASSWORD not set")

  if not check_password(os.environ.get('STREAMLIT_PASSWORD')):
    st.stop()

  data = load_data()

  st.table(data)
